-- Wezterm config

local wez = require('wezterm')

local function get_tab_title(tab)
  local prefix_icon = wez.nerdfonts.dev_terminal
  local arrow_icon = wez.nerdfonts.md_arrow_expand_right

  local active_pane = tab.active_pane
  local process_path = active_pane.foreground_process_name

  local nvim_exec_dir = ''
  local env = ': env TERM=wezterm nvim'
  if string.find(active_pane.title, env, 0, true) then
    nvim_exec_dir = active_pane.title:gsub(env, '')
  end

  if process_path and #process_path > 0 then
    local process_name = string.gsub(process_path, '(.*[/\\])(.*)', '%2')
    local title = ''
    if nvim_exec_dir and #nvim_exec_dir > 0 then
      title = prefix_icon
        .. ' '
        .. process_name
        .. ' '
        .. arrow_icon
        .. ' '
        .. nvim_exec_dir
    else
      title = prefix_icon .. ' ' .. process_name
    end
    return title
  end

  return prefix_icon .. ' ' .. tab.tab_title
end

---@diagnostic disable-next-line: unused-local
wez.on('format-tab-title', function(tab, tabs, panes, config, hover, max_width)
  local title = get_tab_title(tab)

  local left_edge = wez.nerdfonts.ple_lower_right_triangle
  local right_edge = wez.nerdfonts.ple_upper_left_triangle

  local bg = ''
  local fg = ''
  local edge_fg = ''
  local edge_bg = ''

  local active_bg = '#3D9CE1'
  local active_fg = '#1a1a2e'

  local inact_bg = '#1f2335'
  local inact_fg = '#3D9CE1'

  local hover_bg = '#2B4371'
  local hover_fg = '#3D9CE1'

  if tab.is_active then
    bg = active_bg
    fg = active_fg
    edge_bg = active_fg
    edge_fg = active_bg
  elseif hover then
    bg = hover_bg
    fg = hover_fg
    edge_bg = '#1A1A2E'
    edge_fg = hover_bg
  else
    bg = inact_bg
    fg = inact_fg
    edge_bg = '#1A1A2E'
    edge_fg = inact_bg
  end

  title = '  ' .. wez.truncate_right(title, max_width - 6) .. '  '

  return {
    { Background = { Color = edge_bg } },
    { Foreground = { Color = edge_fg } },
    { Text = left_edge },
    { Background = { Color = bg } },
    { Foreground = { Color = fg } },
    { Text = title },
    { Background = { Color = edge_bg } },
    { Foreground = { Color = edge_fg } },
    { Text = right_edge },
  }
end)

return {
  -- Colorscheme
  color_scheme = 'midnight',
  font = wez.font('InputMono Nerd Font', {
    weight = 'Light',
  }),
  font_size = 12,
  font_rules = {
    {
      italic = true,
      intensity = 'Bold',
      font = wez.font({
        family = 'InputMono Nerd Font',
        weight = 'Bold',
        style = 'Italic',
      }),
    },
    {
      italic = true,
      intensity = 'Half',
      font = wez.font({
        family = 'InputMono Nerd Font',
        weight = 'DemiBold',
        style = 'Italic',
      }),
    },
    {
      italic = true,
      intensity = 'Normal',
      font = wez.font({
        family = 'InputMono Nerd Font',
        style = 'Italic',
      }),
    },
  },
  -- don't warn about missing glyphs
  warn_about_missing_glyphs = false,
  -- disable hinting.
  freetype_load_flags = 'DEFAULT',
  freetype_load_target = 'Light',
  freetype_render_target = 'HorizontalLcd',
  allow_square_glyphs_to_overflow_width = 'WhenFollowedBySpace',
  -- Override colors
  colors = {
    cursor_bg = '#94B4DA',
    cursor_fg = '#1A1A2E',
  },
  -- Cursor
  default_cursor_style = 'BlinkingBlock',
  -- Minimize window padding
  enable_scroll_bar = false,
  underline_position = -2,
  window_padding = {
    left = '13px',
    right = '13px',
    top = '13px',
    bottom = '5px',
  },
  -- Hide tabs when only 1 is active
  hide_tab_bar_if_only_one_tab = true,
  tab_bar_at_bottom = true,
  use_fancy_tab_bar = false,
  tab_max_width = 30,

  visual_bell = {
    fade_in_duration_ms = 75,
    fade_out_duration_ms = 75,
    target = 'CursorColor',
  },

  command_palette_bg_color = '#1E1E39',
  command_palette_fg_color = '#3C4F7A',

  front_end = 'OpenGL',

  -- Don't check for updates
  check_for_updates = false,
}